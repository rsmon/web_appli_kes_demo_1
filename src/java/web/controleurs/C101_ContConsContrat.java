
package web.controleurs;

import dao.consultation.contrat.DaoContrat;
import dao.maj.DaoMaj;
import entites.Contrat;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpSession;

@Named
@RequestScoped

public class C101_ContConsContrat {
  
    @Inject private DaoContrat dao;
    
    private Long       numcont;
  
    private Contrat    contrat;
    
   
    public void ecouteurRecherche(){
    
        contrat= dao.getLeContrat(numcont);   
    }

    public void initSession(){
     FacesContext.getCurrentInstance().getExternalContext().getSession( true );
    }

   
    //<editor-fold defaultstate="collapsed" desc="Getters et Setters">
    
    public Long getNumcont() {
        return numcont;
    }

    public void setNumcont(Long numcont) {
        this.numcont = numcont;
    }
    
    
     public Contrat getContrat() {
        return contrat;
    }

    public void setContrat(Contrat contrat) {
        this.contrat = contrat;
    }
    
    
    //</editor-fold>      
}

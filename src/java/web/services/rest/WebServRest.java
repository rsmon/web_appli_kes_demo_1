package web.services.rest;

import dto.DtoContrat;
import dto.DtoInterventionDunContrat;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

@Stateless
@Path("contrat")
public class WebServRest {
    
    @Inject web.services.methodes.MethodesWeb mw;
     
    @GET
    @Path("infos/{numcont}")
    @Produces({"application/xml","application/json"})
    public DtoContrat getLeContrat(@PathParam("numcont")Long numcont) {
       
        return mw.getLeContrat(numcont);
    }
    
    @GET
    @Path("interventions/{numcont}")
    @Produces({"application/xml","application/json"})
    public List<DtoInterventionDunContrat> getInterventionsContrat(@PathParam("numcont") Long numcont) {
         
        return mw.getInterventionsContrat(numcont);
    }   
}

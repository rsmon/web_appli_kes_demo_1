package bal.contrat;

import dao.consultation.contrat.DaoContrat;
import entites.Contrat;
import javax.inject.Inject;

public class BalContrat {
   
    @Inject private DaoContrat dao;
    
    public Float ecartTotalAnnee(){
    
        Float resu=0F;
        for ( Contrat cont : dao.getTousLesContrats() ){ resu+=cont.ecart(); }   
        return resu;
    }  
}
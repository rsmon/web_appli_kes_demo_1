
package dao.consultation.contrat;

import entites.Contrat;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public class DaoContrat {
    
    
    @PersistenceContext
    private EntityManager em;
    
    
    public Contrat getLeContrat(Long numcont){
      
        Contrat c= em.find(Contrat.class, numcont);
        return c;
    } 
    
    public List<Contrat> getTousLesContrats(){
      return em.createQuery("Select c from Contrat c").getResultList();
    }
    
    public List<Contrat> getLesContratsDeficitaires(Float seuil){
    
     List<Contrat> listeCont= new LinkedList();
     
     for (Contrat cont : getTousLesContrats()){
     
       if (cont.ecart()<-seuil) { listeCont.add(cont);}
     }
     
     
     return listeCont;
    
    }
    
}

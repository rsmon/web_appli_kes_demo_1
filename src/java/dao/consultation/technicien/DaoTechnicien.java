package dao.consultation.technicien;

import entites.Technicien;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public class DaoTechnicien {
    
 @PersistenceContext private EntityManager em;
 
 
 public Technicien getLeTechnicien(Long numTech){
 
     return em.find(Technicien.class, numTech);
 }

}
